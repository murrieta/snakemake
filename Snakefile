"""
Setup: before running the script the following commands have to executed:
$ research # an alias that directs you to the snakemake directory
$ mkdir ZIKA; cd ZIKA # if the folder does not exist
$ ln -s /data/ebel_lab/Reyes/ZIKV/EIT/SA1612273-R3/ raw_data
$ ln -s /data/ebel_lab/Reyes/ZIKV/EIT/trimmed_data/ trimmed_data

samples to test with:
water_S91
D-Ag2-0-7S-46_S116
28-Al2-0-14L-18_S56
28-Ag-2-14M-29_S17
13-input-mix-100K_S13
"""

import os
import sys

# Global Variables
configfile: "config/config.yml"

##############################################
# Trim input fastq files                     #
##############################################

# snakemake ZIKA/trimmed_data/water_S91_R1_001.fastq
rule trim_zika:
    input:
        "{genus}/raw_data/{biosample}_R1_001.fastq.gz",
        "{genus}/raw_data/{biosample}_R2_001.fastq.gz"
    output:
        temp("{genus}/aligned_data/{biosample}_R1_001.fastq"),
        temp("{genus}/aligned_data/{biosample}_R2_001.fastq")
    run:
        command = "cutadapt -a AGATCGGAAGAGC -A AGATCGGAAGAGC -g GCTCTTCCGATCT -G GCTCTTCCGATCT -a AGATGTGTATAAGAGACAG -A AGATGTGTATAAGAGACAG -g CTGTCTCTTATACACATCT -G CTGTCTCTTATACACATCT -q 30,30 --minimum-length 80 -u 5 -o {output[0]} -p {output[1]} {input[0]} {input[1]}"
        print(command)
        shell(command)

###############################
# Align with reference genome #
###############################

def get_index(wildcards):
    return "annotations/{}/{}".format(wildcards.genus, config[wildcards.genus]["INDEX"])

# snakemake annotations/ZIKA/btindex/index.1.bt2
rule bowtie2_create_index:
    input:
        get_index
    output:
        "annotations/{genus}/btindex/index.1.bt2"
    run:
        command = "/apps/bowtie2-2.2.9/bowtie2-build -f {input[0]} annotations/{wildcards.genus}/btindex/index"      
        print(command)
        shell(command)

# snakemake ZIKA/aligned_data/bowtie2/water_S91.sam
rule align_bowtie2:
    input: 
        "{genus}/aligned_data/{biosample}_R1_001.fastq",
        "{genus}/aligned_data/{biosample}_R2_001.fastq",
        "annotations/{genus}/btindex/index.1.bt2"
    output:
        temp("{genus}/aligned_data/bowtie2/{biosample}.sam"),
        temp("{genus}/aligned_data/bowtie2/{biosample}_stats.txt")
    run:
        command = "(/apps/bowtie2-2.2.9/bowtie2 -p 8 --phred33 --rdg 5,2 -I 0 -X 700  --very-sensitive-local --score-min C,120,1 -t -x {} -1 {{input[0]}} -2 {{input[1]}} -S {{output[0]}}) 2> {{output[1]}}".format(config[wildcards.genus]["INDEX"])
	print(command)
	shell(command)

def remove_extension(path):
    return list(path)[0][:-4]

# snakemake ZIKA/aligned_data/mosaik2/water_S91.bam
rule align_mosaik2:
    input: 
        "{genus}/aligned_data/{biosample}_R1_001.fastq",
        "{genus}/aligned_data/{biosample}_R2_001.fastq"
    output:
        temp("{genus}/aligned_data/mosaik2/{biosample}.bam"),
        temp("{genus}/aligned_data/mosaik2/{biosample}.sam")
    run:
        command = "perl /apps/V-Fat/runMosaik2.pl -fq {{input[0]}} -fq2 {{input[1]}} -ref {} -o {} -hs 13 -mmp 0.05 -minp 0.8 -mms -9 -ms 1 -hgop 4 -gop 5 -gep 2 -m all -mfl 250 -st illumina;".format(config[wildcards.genus]["REFERENCE"], remove_extension({output[0]}))
	print(command)
	shell(command)

################################################
# sort depending on aligner and variant_caller #
################################################
def get_sorting_command(variant_caller, output):
    if variant_caller == "lofreq":
        return "java -jar /apps/picard-tools-1.99/SortSam.jar I={input[0]} O={output} SO=coordinate"
    if variant_caller == "vphaser2":
        return "samtools view -bS {{input[0]}} | samtools sort - {}".format(remove_extension(list(output)[0]))

#TODO: Fix so it will work with gatk pipe
# snakemake ZIKA/aligned_data/bowtie2/vphaser2/water_S91_sorted.bam
# snakemake ZIKA/aligned_data/bowtie2/lofreq/water_S91_sorted.bam
rule sort_bowtie2:
    input:
        "{genus}/aligned_data/bowtie2/{biosample}.sam",
        "{genus}/aligned_data/bowtie2/{biosample}_stats.txt"
    output:
        "{genus}/aligned_data/bowtie2/{variant_caller}/{biosample}_sorted.bam"
    run:
        command = get_sorting_command(wildcards.variant_caller, {output})
        print(command)
        shell(command)

# snakemake ZIKA/aligned_data/mosaik2/vphaser2/water_S91_sorted.bam
# snakemake ZIKA/aligned_data/mosaik2/lofreq/water_S91_sorted.bam
rule sort_mosaik2:
    input:
        "{genus}/aligned_data/mosaik2/{biosample}.sam"
    output:
        "{genus}/aligned_data/mosaik2/{variant_caller}/{biosample}_sorted.bam"
    run:
        command = get_sorting_command(wildcards.variant_caller, {output})
        print(command)
        shell(command)

#########################################
# Variant calling pre-processing        #
#########################################
# snakemake ZIKA/processed_data/mosaik2/vphaser2/water_S91_rg_added_sorted.bam
# snakemake ZIKA/processed_data/mosaik2/lofreq/water_S91_rg_added_sorted.bam
rule add_or_replace_read_groups:
    input:
        "{genus}/aligned_data/{aligner}/{variant_caller}/{biosample}_sorted.bam"
    output:
        temp("{genus}/aligned_data/{aligner}/{variant_caller}/{biosample}_rg_added_sorted.bam")
    run:
        if wildcards.variant_caller == "vphaser2" or wildcards.variant_caller == "lofreq":
            command = "java -jar /apps/picard-tools-2.9.4/picard.jar AddOrReplaceReadGroups I={input} O={output} RGID=id RGLB=library RGPL=platform RGPU=machine RGSM=sample"
            print(command)
            shell(command)
        else: # gatk
            # will quit because doesn't produce any output
            print("Unknown variant caller: {}".format(wildcards.variant_caller))


# snakemake ZIKA/processed_data/mosaik2/vphaser2/water_S91_dedupped.bam
rule remove_duplicates_vphaser2:
    input:
        "{genus}/aligned_data/{aligner}/vphaser2/{biosample}_rg_added_sorted.bam"
    output:
        temp("{genus}/aligned_data/{aligner}/vphaser2/{biosample}_dedupped.bam"),
        temp("{genus}/aligned_data/{aligner}/vphaser2/{biosample}_dedupped.bai")
    run:
        command = "java -jar /apps/picard-tools-2.9.4/picard.jar MarkDuplicates I={input[0]} O={output[0]} CREATE_INDEX=true VALIDATION_STRINGENCY=LENIENT REMOVE_DUPLICATES=true M=output.metrics"
        print(command)
        shell(command)

# snakemake ZIKA/processed_data/mosaik2/lofreq/water_S91_dedupped.bam
rule remove_duplicates_lofreq:
    input:
        "{genus}/aligned_data/{aligner}/lofreq/{biosample}_rg_added_sorted.bam"
    output:
        temp("{genus}/aligned_data/{aligner}/lofreq/{biosample}_dedupped.bam"),
        temp("{genus}/aligned_data/{aligner}/lofreq/{biosample}_dedupped.bai")
    run:
        command = "java -jar /apps/picard-tools-2.9.4/picard.jar MarkDuplicates I={input[0]} O={output[0]} CREATE_INDEX=true VALIDATION_STRINGENCY=LENIENT REMOVE_DUPLICATES=true M=output.metrics"
        print(command)
        shell(command)

# snakemake ZIKA/processed_data/mosaik2/gatk/water_S91_dedupped.bam
rule remove_duplicates_gatk:
    input:
        "{genus}/aligned_data/{aligner}/lofreq/{biosample}_rg_added_sorted.bam"
    output:
        temp("{genus}/aligned_data/{aligner}/gatk/{biosample}_dedupped.bam"),
        temp("{genus}/aligned_data/{aligner}/gatk/{biosample}_dedupped.bai")
    run:
        command = "java -jar /apps/picard-tools-2.9.4/picard.jar MarkDuplicates I={input[0]} O={output[0]} CREATE_INDEX=true VALIDATION_STRINGENCY=LENIENT REMOVE_DUPLICATES=true M=output.metrics"
        print(command)
        shell(command)

# snakemake ZIKA/aligned_data/mosaik2/gatk/water_S91_MQ_added.bam
rule reassign_map_qual:
    input:
        "{genus}/aligned_data/{aligner}/gatk/{biosample}_dedupped.bam",
        "{genus}/aligned_data/{aligner}/gatk/{biosample}_dedupped.bai"
    output:
        temp("{genus}/aligned_data/{aligner}/gatk/{biosample}_MQ_added.bam"),
        temp("{genus}/aligned_data/{aligner}/gatk/{biosample}_MQ_added.bai")
    run:
        command = "java -jar /apps/GATK-3.7-0/GenomeAnalysisTK.jar -T PrintReads -R {} -I {{input[0]}} -o {{output[0]}} -rf ReassignOneMappingQuality -RMQF 255 -RMQT 60 -U ALLOW_SEQ_DICT_INCOMPATIBILITY".format(config[wildcards.genus]["REFERENCE"])
        print(command)
        shell(command)

# snakemake ZIKA/aligned_data/mosaik2/lofreq/water_S91_viterbi.bam
rule lofreq_viterbi:
    input:
        "{genus}/aligned_data/{aligner}/lofreq/{biosample}_dedupped.bam",
        "{genus}/aligned_data/{aligner}/lofreq/{biosample}_dedupped.bai"
    output:
        temp("{genus}/aligned_data/{aligner}/lofreq/{biosample}_viterbi.bam")
    run:
        command = "lofreq viterbi -f {} -o {{output}} {{input[0]}}".format(config[wildcards.genus]["REFERENCE"])
        print(command)
        shell(command)

# snakemake ZIKA/aligned_data/mosaik2/lofreq/water_S91_viterbi_indelqual.bam
rule lofreq_indelqual:
    input:
        "{genus}/aligned_data/{aligner}/lofreq/{biosample}_viterbi.bam"
    output:
        temp("{genus}/aligned_data/{aligner}/lofreq/{biosample}_viterbi_indelqual.bam")
    run:
        command = "lofreq indelqual --dindel -f {} -o {{output}} {{input}}".format(config[wildcards.genus]["REFERENCE"])
        print(command)
        shell(command)

# snakemake ZIKA/aligned_data/mosaik2/lofreq/water_S91_viterbi_indelqual_sort.bam
rule sort_viterbi:
    input:
        "{genus}/aligned_data/{aligner}/lofreq/{biosample}_viterbi_indelqual.bam"
    output:
        temp("{genus}/aligned_data/{aligner}/lofreq/{biosample}_viterbi_indelqual_sort.bam"),
        temp("{genus}/aligned_data/{aligner}/lofreq/{biosample}_viterbi_indelqual_sort.bai")
    run:
        command = "samtools sort {input[0]} -o {wildcards.genus}/aligned_data/{wildcards.aligner}/lofreq/{wildcards.biosample}_viterbi_indelqual_sort.bam"
        print(command)
        shell(command)
        command = "samtools index {output[0]} {output[1]}"
        print(command)
        shell(command)

#########################################
# Processing Statistics                 #
#########################################

# snakemake ZIKA/statistics/mosaik2/lofreq/water_S91/water_S91_depth
# snakemake ZIKA/statistics/mosaik2/vphaser2/water_S91/water_S91_depth
# snakemake ZIKA/statistics/mosaik2/gatk/water_S91/water_S91_depth
rule depth_of_coverage:
    input:
        "{genus}/aligned_data/{aligner}/{variant_caller}/{biosample}_dedupped.bam",
        "{genus}/aligned_data/{aligner}/{variant_caller}/{biosample}_dedupped.bai"
    output:
        "{genus}/statistics/{aligner}/{variant_caller}/{biosample}/{biosample}_depth",
        temp("{genus}/statistics/{aligner}/{variant_caller}/{biosample}/{biosample}_depth.sample_cumulative_coverage_counts"),
        temp("{genus}/statistics/{aligner}/{variant_caller}/{biosample}/{biosample}_depth.sample_cumulative_coverage_proportions"),
        temp("{genus}/statistics/{aligner}/{variant_caller}/{biosample}/{biosample}_depth.sample_interval_statistics"),
        temp("{genus}/statistics/{aligner}/{variant_caller}/{biosample}/{biosample}_depth.sample_interval_summary")
    run:
        command = "java -jar /apps/GATK-3.7-0/GenomeAnalysisTK.jar -T DepthOfCoverage --unsafe -R {} -o {{output[0]}} -I {{input[0]}}".format(config[wildcards.genus]["REFERENCE"])
        print(command)
        shell(command)

# snakemake ZIKA/statistics/mosaik2/lofreq/water_S91/water_S91_depth.pdf
# snakemake ZIKA/statistics/mosaik2/vphaser2/water_S91/water_S91_depth.pdf
# snakemake ZIKA/statistics/mosaik2/gatk/water_S91/water_S91_depth.pdf
# snakemake ZIKA/statistics/bowtie2/lofreq/water_S91/water_S91_depth.pdf
# snakemake ZIKA/statistics/bowtie2/vphaser2/water_S91/water_S91_depth.pdf
# snakemake ZIKA/statistics/bowtie2/gatk/water_S91/water_S91_depth.pdf
rule generate_depth_graph:
    input:
        "{genus}/statistics/{aligner}/{variant_caller}/{biosample}/{biosample}_depth"
    output:
        "{genus}/results/{aligner}/{variant_caller}/{biosample}/{biosample}_depth.pdf"
    run:
        command = "Rscript scripts/depth.R {input} {output}"
        print(command)
        shell(command)

# snakemake ZIKA/statistics/mosaik2/vphaser2/water_S91/water_S91_view.txt
# snakemake ZIKA/statistics/mosaik2/lofreq/water_S91/water_S91_view.txt
# snakemake ZIKA/statistics/mosaik2/gatk/water_S91/water_S91_view.txt
rule view_samtools:
    input:
        "{genus}/aligned_data/{aligner}/{variant_caller}/{biosample}_dedupped.bam",
        "{genus}/aligned_data/{aligner}/{variant_caller}/{biosample}_dedupped.bai"
    output:
        "{genus}/statistics/{aligner}/{variant_caller}/{biosample}/{biosample}_view.txt"
    run:
        command = "samtools view -c {input[0]} > {output}"
        print(command)
        shell(command)


# snakemake ZIKA/statistics/mosaik2/vphaser2/water_S91/water_S91_flagstat.txt
# snakemake ZIKA/statistics/mosaik2/lofreq/water_S91/water_S91_flagstat.txt
# snakemake ZIKA/statistics/mosaik2/gatk/water_S91/water_S91_flagstat.txt
rule flagstat_samtools:
    input:
        "{genus}/aligned_data/{aligner}/{variant_caller}/{biosample}_dedupped.bam",
        "{genus}/aligned_data/{aligner}/{variant_caller}/{biosample}_dedupped.bai"
    output:
        "{genus}/statistics/{aligner}/{variant_caller}/{biosample}/{biosample}_flagstat.txt"
    run:
        command = "samtools flagstat {input[0]} > {output}"
        print(command)
        shell(command)

#####################
# Variant calling   #
#####################

# snakemake ZIKA/results/mosaik2/vphaser2/water_S91/ZIKV_PRVABC59_Input_NEW.eb
# snakemake ZIKA/results/bowtie/vphaser2/water_S91/ZIKV_PRVABC59_Input_NEW.eb
rule variant_calls_vphaser2:
    input:
        "{genus}/aligned_data/{aligner}/vphaser2/{biosample}_dedupped.bam",
        "{genus}/aligned_data/{aligner}/vphaser2/{biosample}_dedupped.bai"
    output:
        "{genus}/results/{aligner}/vphaser2/{biosample}/{reference_fasta_file}.covplot.R",
        temp("{genus}/results/{aligner}/vphaser2/{biosample}/{reference_fasta_file}.eb"),
        "{genus}/results/{aligner}/vphaser2/{biosample}/{reference_fasta_file}.fdr.var.txt",
        "{genus}/results/{aligner}/vphaser2/{biosample}/{reference_fasta_file}.nofdr.var.txt",
        "{genus}/results/{aligner}/vphaser2/{biosample}/{reference_fasta_file}.var.raw.txt"
    threads: 8
    run:
        command = "OMP_NUM_THREADS={threads} vphaser2 -i ./{input[0]} -o ./{wildcards.genus}/results/{wildcards.aligner}/vphaser2/{wildcards.biosample};"
        print(command)
        shell(command)
        print("rule complete - now cleaning up")
        shell("rm {}/*.region".format(os.path.dirname(output[0])))
        # index file is created and should be removed at the end of this step
        shell("rm {input[0]}.bti")

# snakemake ZIKA/results/mosaik2/lofreq/water_S91/water_S91.vcf
rule variant_calls_lofreq:
    input:
        "{genus}/aligned_data/{aligner}/lofreq/{biosample}_viterbi_indelqual_sort.bam",
        "{genus}/aligned_data/{aligner}/lofreq/{biosample}_viterbi_indelqual_sort.bai"
    output:
        "{genus}/results/{aligner}/lofreq/{biosample}/{biosample}.vcf"
    run:
        command = "lofreq call --call-indels -f {} -o {{output}} {{input[0]}}".format(config[wildcards.genus]["REFERENCE"])
        print(command)
        shell(command)

#TODO: fix command error, this is a GATK specific error.
# snakemake ZIKA/results/mosaik2/gatk/water_S91/water_S91.vcf
rule variant_calls_gatk:
    input:
        "{genus}/aligned_data/{aligner}/gatk/{biosample}_MQ_added.bam",
        "{genus}/aligned_data/{aligner}/gatk/{biosample}_MQ_added.bai"
    output:
        "{genus}/results/{aligner}/gatk/{biosample}/{biosample}.vcf",
        "{genus}/results/{aligner}/gatk/{biosample}/{biosample}.vcf.idx"
    run:
        command = "java -jar /apps/GATK-3.7-0/GenomeAnalysisTK.jar -T HaplotypeCaller -R {} -I {{input[0]}} -dontUseSoftClippedBases -stand_call_conf 20.0 -o {{output[0]}}".format(config[wildcards.genus]["REFERENCE"])
        print(command)
        shell(command)


#########################################
# Variant calling Post processing       #
#########################################

# snakemake ZIKA/results/mosaik2/lofreq/water_S91/water_S91_filter.vcf
rule lofreq_filter:
    input:        
        "{genus}/results/{aligner}/lofreq/{biosample}/{biosample}.vcf"
    output:
        "{genus}/results/{aligner}/lofreq/{biosample}/{biosample}_filter.vcf"
    run:
        command = "lofreq filter -a 0.01 -i {input[0]} -o {output[0]}"
        print(command)
        shell(command)

# TODO: this uses GATK - should it be part of the lofreq pipeline?
# snakemake ZIKA/results/mosaik2/lofreq/water_S91/water_S91_results.table
rule variants_to_tables_lofreq:
    input:
        "{genus}/results/{aligner}/lofreq/{biosample}/{biosample}.vcf"
    output:
        "{genus}/results/{aligner}/lofreq/{biosample}/{biosample}_results.table"
    run:
        command = "java -jar /apps/GATK-3.7-0/GenomeAnalysisTK.jar -R {} -T VariantsToTable -V {{input[0]}} -F CHROM -F POS -F FILTER -F ID -F AC -F TRANSITION -F REF -F ALT -F QUAL -GF DP -F FS -F TYPE -F AF -F AN -F SB -AMD -SMA -GF GT -GF AF -o {{output[0]}}".format(config[wildcards.genus]["REFERENCE"])
        print(command)
        shell(command)

# snakemake ZIKA/results/mosaik2/gatk/water_S91/water_S91_results.table
rule variants_to_tables_gatk:
    input:
        "{genus}/results/{aligner}/gatk/{biosample}/{biosample}.vcf"
    output:
        "{genus}/results/{aligner}/gatk/{biosample}/{biosample}_results.table"
    run:
        command = "java -jar /apps/GATK-3.7-0/GenomeAnalysisTK.jar -R {} -T VariantsToTable -V {{input[0]}} -F CHROM -F POS -F FILTER -F ID -F AC -F TRANSITION -F REF -F ALT -F QUAL -GF DP -F FS -F GT -F TYPE -F AF -F AN -F SB -AMD -SMA -GF GT -o {{output[0]}}".format(config[wildcards.genus]["REFERENCE"])
        print(command)
        shell(command)

# snakemake ZIKA/results/mosaik2/lofreq/water_S91/water_S91_consensus.vcf
rule lofreq_majority:
    input:
        "{genus}/results/{aligner}/lofreq/{biosample}/{biosample}.vcf"
    output:
        "{genus}/results/{aligner}/lofreq/{biosample}/{biosample}_consensus.vcf"
    run:
        command = "lofreq filter -a 0.50 -v 100 -i {input[0]} -o {output[0]}"
        print(command)
        shell(command)

# snakemake ZIKA/results/mosaik2/lofreq/water_S91/water_S91_minority.vcf
rule lofreq_minority:
    input:
        "{genus}/results/{aligner}/lofreq/{biosample}/{biosample}.vcf"
    output:
        "{genus}/results/{aligner}/lofreq/{biosample}/{biosample}_minority.vcf"
    run:
        command = "lofreq filter -A 0.49 -v 100 -i {input[0]} -o {output[0]}"
        print(command)
        shell(command)

# snakemake ZIKA/results/mosaik2/lofreq/water_S91/water_S91_consensus.vcf.gz
rule bg_zip:
    input:
        "{genus}/results/{aligner}/lofreq/{biosample}/{biosample}_consensus.vcf"
    output:
        "{genus}/results/{aligner}/lofreq/{biosample}/{biosample}_consensus.vcf.gz"
    run:
        command = "bgzip {input[0]}"
        print(command)
        shell(command)

# snakemake ZIKA/results/mosaik2/lofreq/water_S91/water_S91_consensus.vcf.gz.tbi
rule tabix:
    input:
        "{genus}/results/{aligner}/lofreq/{biosample}/{biosample}_consensus.vcf.gz"
    output:
        "{genus}/results/{aligner}/lofreq/{biosample}/{biosample}_consensus.vcf.gz.tbi"
    run:
        command = "tabix -p vcf {input[0]}"
        print(command)
        shell(command)

# This ouput is the results.table in the {genus}/results/{aligner}/{variant_caller}/{biosample}/
# snakemake ZIKA/results/mosaik2/lofreq/water_S91/water_S91_consensus.fasta
rule vcf_consensus_file:
    input:
        "{genus}/results/{aligner}/lofreq/{biosample}/{biosample}_consensus.vcf.gz",
        "{genus}/results/{aligner}/lofreq/{biosample}/{biosample}_consensus.vcf.gz.tbi"
    output: 
        "{genus}/results/{aligner}/lofreq/{biosample}/{biosample}_consensus.fasta"
    run:
        command = "cat {} | vcf-consensus {{input[0]}} > {{output[0]}}".format(config[wildcards.genus]["REFERENCE"])
        print(command)
        shell(command)


################################
# Variant callers for aligners #
################################


def get_header_name(file):
    with open(file) as fin:
        header = fin.readline()
        return header.strip()[1:]


def generate_vphaser2_output(wildcards):
    genus = wildcards.genus
    aligner = wildcards.aligner
    biosample = wildcards.biosample
    base_directory = "{0}/statistics/{1}/vphaser2/{2}/{2}".format(genus, aligner, biosample)

    results = ["{}/results/{}/vphaser2/{}/{}.eb".format(genus, aligner, biosample, get_header_name(config[genus]["REFERENCE"])),
               "{}_view.txt".format(base_directory),
               "{}_flagstat.txt".format(base_directory),
               "{}_depth".format(base_directory),
               "{}_depth.sample_cumulative_coverage_counts".format(base_directory),
               "{}_depth.sample_cumulative_coverage_proportions".format(base_directory),
               "{}_depth.sample_interval_statistics".format(base_directory),
               "{}_depth.sample_interval_summary".format(base_directory),
               "{0}/results/{1}/vphaser2/{2}/{2}_depth.pdf".format(genus, aligner, biosample)
               ]
    return results


# snakemake flags/ZIKA/mosaik2/samples/water_S91/vphaser2
# snakemake flags/ZIKA/bowtie2/samples/water_S91/vphaser2
rule vphaser2_single_sample:
    input: generate_vphaser2_output
    output:
        touch("flags/{genus}/{aligner}/samples/{biosample}/vphaser2")


# snakemake flags/ZIKA/mosaik2/samples/water_S91/lofreq
# snakemake flags/ZIKA/bowtie2/samples/water_S91/lofreq
rule lofreq_single_sample:
    input:
        "{genus}/results/{aligner}/lofreq/{biosample}/{biosample}_results.table",
        "{genus}/results/{aligner}/lofreq/{biosample}/{biosample}_consensus.fasta",
        "{genus}/statistics/{aligner}/lofreq/{biosample}/{biosample}_view.txt",
        "{genus}/statistics/{aligner}/lofreq/{biosample}/{biosample}_flagstat.txt",
        "{genus}/statistics/{aligner}/lofreq/{biosample}/{biosample}_depth",
        "{genus}/statistics/{aligner}/lofreq/{biosample}/{biosample}_depth.sample_cumulative_coverage_counts",
        "{genus}/statistics/{aligner}/lofreq/{biosample}/{biosample}_depth.sample_cumulative_coverage_proportions",
        "{genus}/statistics/{aligner}/lofreq/{biosample}/{biosample}_depth.sample_interval_statistics",
        "{genus}/statistics/{aligner}/lofreq/{biosample}/{biosample}_depth.sample_interval_summary",
        "{genus}/results/{aligner}/lofreq/{biosample}/{biosample}_depth.pdf"
    output:
        touch("flags/{genus}/{aligner}/samples/{biosample}/lofreq")


# snakemake flags/ZIKA/mosaik2/samples/water_S91/gatk
# snakemake flags/ZIKA/bowtie2/samples/water_S91/gatk
rule gatk_single_sample:
    input:
        "{genus}/results/{aligner}/gatk/{biosample}/{biosample}_results.table",
        "{genus}/statistics/{aligner}/gatk/{biosample}/{biosample}_flagstat.txt",
        "{genus}/statistics/{aligner}/gatk/{biosample}/{biosample}_view.txt",
        "{genus}/statistics/{aligner}/gatk/{biosample}/{biosample}_depth",
        "{genus}/statistics/{aligner}/gatk/{biosample}/{biosample}_depth.sample_cumulative_coverage_counts",
        "{genus}/statistics/{aligner}/gatk/{biosample}/{biosample}_depth.sample_cumulative_coverage_proportions",
        "{genus}/statistics/{aligner}/gatk/{biosample}/{biosample}_depth.sample_interval_statistics",
        "{genus}/statistics/{aligner}/gatk/{biosample}/{biosample}_depth.sample_interval_summary",
        "{genus}/results/{aligner}/gatk/{biosample}/{biosample}_depth.pdf"
    output:
        touch("flags/{genus}/{aligner}/samples/{biosample}/gatk")


def gather_samples(wildcards):
    filename = config[wildcards.genus]["SAMPLE_LIST"]
    with open(filename) as f:
        return f.readlines()

def format_samples_for_single_variant(wildcards):
    biosamples = gather_samples(wildcards)
    g = wildcards.genus
    a = wildcards.aligner
    v = wildcards.variant_caller
    samples = []
    for s in biosamples:
        samples.append("flags/{}/{}/samples/{}/{}".format(g, a, s.strip(), v))
    return samples


# snakemake flags/ZIKA/mosaik2/vphaser2_complete
# snakemake flags/ZIKA/mosaik2/lofreq_complete
# snakemake flags/ZIKA/mosaik2/gatk_complete
# snakemake flags/ZIKA/bowtie2/vphaser2_complete
# snakemake flags/ZIKA/bowtie2/lofreq_complete
# snakemake flags/ZIKA/bowtie2/gatk_complete
rule single_variant_caller_all_samples:
    input: format_samples_for_single_variant
    output: touch("flags/{genus}/{aligner}/{variant_caller}_complete")


#############################
# run scripts and analytics #
#############################
# NOTE: Change {variance_caller} to {variant_caller} to match processing code. 

# snakemake ZIKA/results/mosaik2/lofreq/all_samples.csv
# snakemake ZIKA/results/mosaik2/vphaser2/all_samples.csv
# snakemake ZIKA/results/bowtie2/lofreq/all_samples.csv
# snakemake ZIKA/results/bowtie2/vphaser2/all_samples.csv
rule combine_all_samples:
    input:
        "flags/{genus}/{aligner}/{variance_caller}_complete"
    output:
        "{genus}/results/{aligner}/{variance_caller}/all_samples.csv"
    run:
        input_file = "results.table" if wildcards.variance_caller == "lofreq" else "ZIKV_PRVABC59_Input_NEW.var.raw.txt"
        directory = "{}/results/{}/{}/".format(wildcards.genus, wildcards.aligner, wildcards.variance_caller)
        sample_list = config[wildcards.genus]["SAMPLE_LIST"]
        command = "python scripts/gather_data.py -s {} --variant-directory {} --variants-filename {} --depths-directory {} -o {{output}}".format(sample_list, directory, input_file, directory.replace("results", "statistics"))
        print(command)
        shell(command)

# snakemake ZIKA/results/mosaik2/lofreq/sample_analysis.csv
# snakemake ZIKA/results/mosaik2/vphaser2/sample_analysis.csv
# snakemake ZIKA/results/bowtie2/lofreq/sample_analysis.csv
# snakemake ZIKA/results/bowtie2/vphaser2/sample_analysis.csv
rule sample_analysis:
    input:
        "{genus}/results/{aligner}/{variance_caller}/all_samples.csv"
    output:
        "{genus}/results/{aligner}/{variance_caller}/sample_analysis.csv"
    run:
        character = 'p' if wildcards.variance_caller == 'vphaser2' else 'l'
        depths_file = "{}/statistics/{}/{}".format(wildcards.genus, wildcards.aligner, wildcards.variance_caller)
        start = config[wildcards.genus]["START_CODON"]
        stop = config[wildcards.genus]["STOP_CODON"]
        command = "python scripts/sample_analysis.py -{} {{input}} -d {} --start {} --stop {} -o {{output}}".format(character, depths_file, start, stop)
        print(command)
        shell(command)

# snakemake ZIKA/results/mosaik2/lofreq/nucleotide_analysis.csv
# snakemake ZIKA/results/mosaik2/vphaser2/nucleotide_analysis.csv
# snakemake ZIKA/results/bowtie2/lofreq/nucleotide_analysis.csv
# snakemake ZIKA/results/bowtie2/vphaser2/nucleotide_analysis.csv
rule nucleotide_analysis:
    input:
        "{genus}/results/{aligner}/{variance_caller}/all_samples.csv"
    output:
        "{genus}/results/{aligner}/{variance_caller}/nucleotide_analysis.csv"
    run:
        character = 'p' if wildcards.variance_caller == 'vphaser2' else 'l'
        start = config[wildcards.genus]["START_CODON"]
        stop = config[wildcards.genus]["STOP_CODON"]
        command = "python scripts/nucleotide_analysis.py -{} {{input}} --start {} --stop {} -o {{output}}".format(character, start, stop)
        print(command)
        shell(command)

# snakemake ZIKA/results/mosaik2/lofreq/dN_dS_ratio.csv
# snakemake ZIKA/results/mosaik2/vphaser2/dN_dS_ratio.csv
# snakemake ZIKA/results/bowtie2/lofreq/dN_dS_ratio.csv
# snakemake ZIKA/results/bowtie2/vphaser2/dN_dS_ratio.csv
rule dN_dS_ratio:
    input:
        "{genus}/results/{aligner}/{variance_caller}/nucleotide_analysis.csv",
        "{genus}/results/{aligner}/{variance_caller}/mutation_analysis.csv"
    output:
        "{genus}/results/{aligner}/{variance_caller}/dN_dS_ratio.csv"
    run:
        start = config[wildcards.genus]["START_CODON"]
        stop = config[wildcards.genus]["STOP_CODON"]
	command = "python scripts/dN-dS-ratio.py -nf {{input[0]}} -mf {{input[1]}} --start {} --stop {} -o {{output}}".format(start, stop)
        print(command)
        shell(command)


# snakemake ZIKA/results/mosaik2/lofreq/mutation_analysis.csv
# snakemake ZIKA/results/mosaik2/vphaser2/mutation_analysis.csv
# snakemake ZIKA/results/bowtie2/lofreq/mutation_analysis.csv
# snakemake ZIKA/results/bowtie2/vphaser2/mutation_analysis.csv
rule find_mutations:
    input:
        "{genus}/results/{aligner}/{variance_caller}/nucleotide_analysis.csv"
    output:
        "{genus}/results/{aligner}/{variance_caller}/mutation_analysis.csv"
    run:
        character = 'p' if wildcards.variance_caller == 'vphaser2' else 'l'
        model = config[wildcards.genus]['MODEL']
        command = "python scripts/find_mutations.py -{} {{input}} -f {} -o {{output}}".format(character, model)
        print(command)
        shell(command)

# snakemake ZIKA/results/mosaik2/lofreq/fst.csv
# snakemake ZIKA/results/mosaik2/vphaser2/fst.csv
# snakemake ZIKA/results/bowtie2/lofreq/fst.csv
# snakemake ZIKA/results/bowtie2/vphaser2/fst.csv
rule calculate_fst:
    input:
        "{genus}/results/{aligner}/{variance_caller}/nucleotide_analysis.csv"
    output:
        "{genus}/results/{aligner}/{variance_caller}/fst.csv"
    run:
        reference = config[wildcards.genus]['FST_REFERENCE']
        command = "Rscript scripts/fst.R {{input}} {{output}} {}".format(reference)
        print(command)
        shell(command)


# snakemake flags/ZIKA/mosaik2/vphaser2_analysis_complete
# snakemake flags/ZIKA/mosaik2/lofreq/vphaser2_analysis_complete
# snakemake flags/ZIKA/bowtie2/vphaser2/vphaser2_analysis_complete
# snakemake flags/ZIKA/bowtie2/lofreq/vphaser2_analysis_complete
rule all_calculations:
    input:
        "{genus}/results/{aligner}/{variance_caller}/all_samples.csv",
        "{genus}/results/{aligner}/{variance_caller}/sample_analysis.csv",
        "{genus}/results/{aligner}/{variance_caller}/nucleotide_analysis.csv",
        "{genus}/results/{aligner}/{variance_caller}/mutation_analysis.csv",
        "{genus}/results/{aligner}/{variance_caller}/dN_dS_ratio.csv",
        "{genus}/results/{aligner}/{variance_caller}/fst.csv"
    output:
        touch("flags/{genus}/{aligner}/{variance_caller}_analysis_complete")
