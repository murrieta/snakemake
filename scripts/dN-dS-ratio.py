"""
A script for calculating the dN-dS ratio

python scripts/dN-dS-ratio.py -nf ZIKA/results/mosaik2/lofreq/nucleotide_analysis.csv -mf ZIKA/results/mosaik2/lofreq/mutation_analysis.csv -o ZIKA/results/mosaik2/lofreq/dN-dF.csv
"""

from argparse import ArgumentParser
import math


def sum_alelle_frequencies(nucleotide_file, mutation_file, start_position, stop_position):
    samples = {}
    with open(nucleotide_file) as nf, open(mutation_file) as mf:
        nf.readline()
        for line in nf:
            data = line.split(",")
            if (start_position is not None and stop_position is not None and
                    start_position <= int(data[1]) <= stop_position):
                if data[0] not in samples:
                    samples[data[0]] = {"S": 0, "NS": 0, "INDEL": 0}
                if data[-3] in ("indel", "lp"):
                    samples[data[0]]["INDEL"] += float(data[-2])
        mf.readline()
        for line in mf:
            data = line.strip().split(",")
            if (start_position is not None and stop_position is not None and
                    start_position <= int(data[1]) <= stop_position):
                if data[-3] == "S":
                    samples[data[0]]["S"] += float(data[-1])
                if data[-3] == "NS":
                    samples[data[0]]["NS"] += float(data[-1])
    return samples


def calculate_dN_and_dS(samples, nSites, sSites):
    for s in samples:
        pN = (samples[s]["NS"] + samples[s]["INDEL"]) / nSites
        samples[s]["dN"] = -3 * math.log(1 - 4 * pN / 3) / 4

        pS = samples[s]["S"] / sSites
        dS = -3 * math.log(1 - 4 * pS / 3) / 4
        samples[s]["dS"] = dS if dS != 0 else 1
    return samples


def format_output(samples):
    from operator import itemgetter
    results = [[s, samples[s]["dN"], samples[s]["dS"], samples[s]["dN"]/samples[s]["dS"]] for s in samples]
    return sorted(results, key=itemgetter(0))


def main():
    parser = ArgumentParser(description="Further analysis on single nucleotide polymorphisms")
    parser.add_argument("--synPos", dest="synPos", type=float, default=2446.17)
    parser.add_argument("--nSynPos", dest="nSynPos", type=float, default=7822.83)
    parser.add_argument("--start", dest="start_codon", default=None, type=int, help="codon position to start analyzing")
    parser.add_argument("--stop", dest="stop_codon", default=None,  type=int, help="codon position to stop analyzing")

    required = parser.add_argument_group("required arguments")
    required.add_argument("-nf", dest="nucleotide_file", type=str, required=True,
                          help="location of the nucleotide analysis")
    required.add_argument("-mf", dest="mutation_file", type=str, required=True,
                          help="location of mutation analysis")
    required.add_argument("-o", dest="output_file", required=True, help="directory and file for output")

    args = parser.parse_args()

    samples = sum_alelle_frequencies(args.nucleotide_file, args.mutation_file, args.start_codon, args.stop_codon)
    samples = calculate_dN_and_dS(samples, args.nSynPos, args.synPos)

    with open(args.output_file, "w") as out:
        out.write("sample,dN,dS,dN/dS ratio\n")
        for s in format_output(samples):
            out.write("{}\n".format(",".join(map(str, s))))


if __name__ == "__main__":
    main()
