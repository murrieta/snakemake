from argparse import ArgumentParser
import sys
import os

if sys.version_info[0] < 3:
    raise Exception("python 3.0.0 or a more recent version is required.")

"""
Processing for vphaser and lofreq output files with a file of depths.

V-PHASER:
** has header **
pos, var, alt, strd_bias, type, var_perc, snp_or_lp_profile

1. Read file into a dictionary and calculate the frequency (var_perc/100)
2. Remove variants with a 108 < pos < 10379 (or as specified). These are in the 5'UTR and 3'UTR region.
3. Remove any variants with a pval > 0.05 (or as specified)
4. Calculate average coverage in depths file for variants with a 108 < pos < 10379 (or as specified)
5. Calculate Shannon entropy
6. filter according to specific allele frequency (if specified)

LO-FREQ:
** has header **
pos, filter, ref, alt, qual, type,  , sb
1. Read file into a dictionary
2. Remove variants with a 108 < pos < 10379. These are in the 5'UTR and 3'UTR region.
4. Calculate average coverage in depths file for variants with a 108 < pos < 10379
6. Calculate Shannon entropy


Example of how to run the program:
vphaser2:
     python scripts/nucleotide_analysis.py -p ZIKA/results/mosaik2/vphaser2/all_samples.csv -o ZIKA/results/mosaik2/vphaser2/nucleotide_analysis.py

lofreq:
     python scripts/nucleotide_analysis.py -l ZIKA/results/mosaik2/lofreq/all_samples.csv -o ZIKA/results/mosaik2/lofreq/nucleotide_analysis.py

additional arguments:
    --p-value .5
    --frequency .5
"""
vphaser_header = ["sample", "pos", "alt", "ref", "strd_bias_pval", "coverage",
                  "type", "var_percent", "af", "snp_or_lp_profile"]
lofreq_header = ["sample", "pos", "filter", "ref", "alt",
                 "coverage", "qual", "type", "af", "sb"]


def process_vphaser_file(line):
    """ process lines of vphaser file and calculate var_freq"""
    results = line.strip().split(",")
    return [results[0], int(results[1]), results[2].lower(), results[3].lower(),
            float(results[4]), int(results[5]), results[6].lower(), float(results[7]),
            float(results[7])/100, ",".join(results[8:])]


def process_lofreq_table(line):
    results = line.strip().split(",")
    return [results[0], int(results[2]), results[3].lower(), results[8].lower(),
            results[9].lower(), int(results[5]), float(results[10]), results[12].lower(),
            float(results[13]), int(results[15])]


def input_to_dictionary(header, data):
    samples = {}
    for line in data:
        # dictionary of dictionaries
        if line[0] not in samples:
            samples[line[0]] = {}
        if line[1] not in samples[line[0]]:
            samples[line[0]][line[1]] = [{header[i].lower(): line[i] for i in range(len(header))}]
        else:
            samples[line[0]][line[1]].append({header[i].lower(): line[i] for i in range(len(header))})
    return samples


def filter_by_position(data, start, stop):
    """
    Removes variants that are outside of a specific range or under a certain p_value
    :param data: the data to be filtered
    :param start: the starting position
    :param stop: the ending position
    :return: a list inclusively within the range of start and stop
    """
    results = {}
    for sample in data:
        if sample not in results: results[sample] = {}
        for pos in data[sample]:
            if start <= pos <= stop:
                results[sample][pos] = data[sample][pos]
    return results


def filter_by_label(data, filter_label, value):
    """
    :param data: a dictionary of lists of dictionaries - sample{nucleotide_position[variants_in_position]}
    :param filter_label: which label to filter by
    :param value: the value to filter by
    :return: a new copy of the filtered dictionary
    """
    results = {}
    for sample in data:
        if sample not in results: results[sample] = {}
        for pos in data[sample]:
            if pos not in results[sample]: results[sample][pos] = []
            for variant in data[sample][pos]:
                if variant[filter_label] <= value:
                    results[sample][pos].append(variant)
    return results


def calculate_shannon_entropy(p):
    """
    will not work if p is 0 because log of 0 is an illegal operation
    :param p: an allele frequency
    :return: returns the shannon entropy of a specific allele frequency
    """
    from math import log, isclose
    return -(((1-p) * log((1-p), 2)) + (p*log(p, 2))) if not isclose(1, p) else 0


def dictionary_to_list(results_dict, header):
    """
    takes a dictionary and returns a list
    :param results_dict: dictionary that includes the final calculations for each variant
    :param header: labels for vphaser2 or lofreq, this is the order the dictionary will be ordered.
    :return: list of the dictionary results - should be ordered by sample
    """
    results_list = []
    for sample in results_dict:
        for pos in results_dict[sample]:
            for variant in results_dict[sample][pos]:
                results_list += [[variant[h] for h in header]]
    return results_list


def write_matrix(output, results_list):
    with open(os.path.join(output), "w") as table_out:
        for result in results_list:
            table_out.write("{}\n".format(",".join(map(str, result))))


def main():
    # fill in programs that generate the results files
    parser = ArgumentParser(description="takes output from the gather_data script")
    parser.add_argument("-p", "-vphaser", dest="vphaser_file", type=str, default=None,
                        help="location of the variant vphaser file")
    parser.add_argument("-l", "-lofreq", dest="lofreq_file", default=None,
                        help="location of the variant lofreq output")
    parser.add_argument("--p-value", dest="p_value", default=None, type=float,
                        help="p-value to filter by")
    parser.add_argument("--frequency", dest="allele_frequency", default=None, type=float,
                        help="allele frequency to filter by")
    # required arguments
    required = parser.add_argument_group("required arguments")
    required.add_argument("--start", dest="start_codon", required=True, type=int, help="codon position to start analyzing")
    required.add_argument("--stop", dest="stop_codon", required=True, type=int, help="codon position to stop analyzing")
    required.add_argument("-o", "--output", dest="output", required=True, help="output path and filename")
    args = parser.parse_args()

    # One of the following files needs to exist, but not both
    if bool(args.vphaser_file) == bool(args.lofreq_file):
        print("Input from either vphaser or lofreq must be specified, but not both.")
        sys.exit()

    path = os.path.dirname(args.output)
    if not os.path.exists(path):
        os.makedirs(path)

    # gather the data
    if args.vphaser_file:
        with open(args.vphaser_file) as f:
            f.readline()  # discard header
            header = vphaser_header
            data = [process_vphaser_file(d) for d in f.readlines()]
    else:
        with open(args.lofreq_file) as f:
            f.readline()  # discard header
            header = lofreq_header
            data = [process_lofreq_table(d) for d in f.readlines()]

    results_dict = input_to_dictionary(header, data)

    # filter
    results_dict = filter_by_position(results_dict, args.start_codon, args.stop_codon)

    if args.p_value:
        results_dict = filter_by_label(results_dict, "strd_bias_pval", args.p_value)

    if args.allele_frequency:
        results_dict = filter_by_label(results_dict, "af", args.allele_frequency)

    for sample in results_dict:
        for pos in results_dict[sample]:
            for variant in results_dict[sample][pos]:
                entropy = calculate_shannon_entropy(variant["af"])
                variant["entropy (S)"] = entropy

    # This file includes anything that has to do with individual samples
    header_output = (["sample", "pos", "alt", "ref", "strd_bias_pval", "coverage",
                     "entropy (S)", "type", "var_percent", "af", "snp_or_lp_profile"]
                     if args.vphaser_file else
                     ["sample", "pos", "alt", "ref", "filter", "qual", "coverage",
                      "entropy (S)", "type", "af", "sb"])
    results_list = dictionary_to_list(results_dict, header_output)
    write_matrix(args.output, [header_output] + results_list)


if __name__ == "__main__":
    main()
