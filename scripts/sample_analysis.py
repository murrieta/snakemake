from argparse import ArgumentParser
import sys
import os
# calculate the richness
# richness = calculate_richness(results_dict)
# average_coverage = calculate_variant_coverage(depths, args.start, args.stop)
# normalized_richness = normalize_richness(richness, average_coverage)

# calculate shannon entropy of each sample and totals for
# all samples, SNV variants, and length variants
"""
a script that gives analysis for each sample

Example of how to run the program:
vphaser2:
     python scripts/sample_analysis.py -p ZIKA/results/vphaser2/all_samples.csv -d ZIKA/statistics/vphaser2 -o ZIKA/results/vphaser2/sample_analysis.csv

lofreq:
     python scripts/sample_analysis.py -l results/ZIKA/lofreq/all_samples.csv -o results/ZIKA/lofreq/sample_analysis.csv

"""

vphaser_header = ["sample", "pos", "alt", "ref", "strd_bias_pval", "coverage",
                  "type", "var_percent", "af", "snp_or_lp_profile"]
lofreq_header = ["sample", "pos", "filter", "ref", "alt",
                 "coverage", "qual", "type", "af", "sb"]


def process_vphaser_file(line):
    """ process lines of vphaser file and calculate var_freq"""
    results = line.strip().split(",")
    return [results[0], int(results[1]), results[2].lower(), results[3].lower(),
            float(results[4]), int(results[5]), results[6].lower(), float(results[7]),
            float(results[7])/100, ",".join(results[8:])]


def process_lofreq_table(line):
    results = line.strip().split(",")
    return [results[0], int(results[2]), results[3].lower(), results[8].lower(),
            results[9].lower(), int(results[5]), float(results[10]), results[12].lower(),
            float(results[13]), int(results[15])]


def input_to_dictionary(header, data, start_codon, stop_codon):
    samples = {}
    for line in data:
        # dictionary of dictionaries
        if start_codon <= int(line[1]) <= stop_codon:
            if line[0] not in samples:
                samples[line[0]] = {}
            if line[1] not in samples[line[0]]:
                samples[line[0]][line[1]] = [{header[i].lower(): line[i] for i in range(len(header))}]
            else:
                samples[line[0]][line[1]].append({header[i].lower(): line[i] for i in range(len(header))})
    return samples


def calculate_variant_coverage(data, results):
    """
      calculates average coverage of variants in the CDS.
      formula: sum(coverage) per variant / number of variants
    """
    for sample in data:
        for pos in data[sample]:
            if "total variant coverage" in results[sample]:
                results[sample]["total variant coverage"] += data[sample][pos][0]["coverage"]
                results[sample]["count"] += 1
            else:
                results[sample]["total variant coverage"] = data[sample][pos][0]["coverage"]
                results[sample]["count"] = 1
            results[sample]["coverage (variants)"] = results[sample]["total variant coverage"] / results[sample]["count"]
    return results


def calculate_cds_coverage(directory, results, start, stop):

    def filter_coverage(line):
        l = line.strip().split("\t")
        return int(l[-1]) if start < int(l[0].split(":")[1]) < stop else 0

    for sample in results:
        with open(os.path.join(directory, sample, sample + "_depth")) as f:
            f.readline()  # throw away the header
            results[sample]["coverage (cds)"] = sum([filter_coverage(c) for c in f.readlines()]) / (stop - start)
    return results


def calculate_richness_per_sample(data, results):
    """
    Richness per sample. There are three types of richness: total, snp, length.
    :param results:
    :param data:
    :return: the sample dictionary with richness values added
    """
    # setup count for richness in each sample
    for sample in results:
        results[sample]["richness"] = 0
        results[sample]["richness (snp)"] = 0
        results[sample]["richness (lp)"] = 0
    # calculate the richness per type for each sample
    for sample in data:
        for pos in data[sample]:
            for variant in data[sample][pos]:
                results[sample]["richness"] += 1
                if variant["type"] in ("lp", "indel"):
                    results[sample]["richness (lp)"] += 1
                if variant["type"] == "snp":
                    results[sample]["richness (snp)"] += 1
    return results


# Assumes that Shannon entropy has not been calculated yet, shannon entropy is derived via the allele frequency
def calculate_complexity_per_sample(data, results, possible_genomic_positions):

    def calculate_shannon_entropy(p):
        from math import log, isclose
        return -(((1 - p) * log((1 - p), 2)) + (p * log(p, 2))) if not isclose(p, 1.0) else 0

    # setup the sample dictionary
    for sample in results:
        results[sample]["complexity (cds)"] = 0
        results[sample]["complexity (snp)"] = 0
        results[sample]["complexity (lp)"] = 0

    # calculate the complexity per type per sample
    for sample in data:
        for pos in data[sample]:
            for variant in data[sample][pos]:
                results[sample]["complexity (cds)"] += calculate_shannon_entropy(variant["af"])
                if variant["type"] == "snp":
                    results[sample]["complexity (snp)"] += calculate_shannon_entropy(variant["af"])
                if variant["type"] in ("lp", "indel"):
                    results[sample]["complexity (lp)"] += calculate_shannon_entropy(variant["af"])

    # divide by the possible genomic positions
    for sample in results:
        results[sample]["complexity (cds)"] /= possible_genomic_positions
        results[sample]["complexity (snp)"] /= possible_genomic_positions
        results[sample]["complexity (lp)"] /= possible_genomic_positions

    return results


def sum_alelle_frequencies(data, results):
    # sum af per sample and for each variant
    for sample in results:
        results[sample]["af (cds)"] = 0
        results[sample]["af (snp)"] = 0
        results[sample]["af (lp)"] = 0

    for sample in data:
        for pos in data[sample]:
            for variant in data[sample][pos]:
                results[sample]["af (cds)"] += variant["af"]
                if variant["type"] == "snp":
                    results[sample]["af (snp)"] += variant["af"]
                if variant["type"] in ("lp", "indel"):
                    results[sample]["af (lp)"] += variant["af"]
    return results


def calculate_nucleotide_diversity(results, possible_genomic_positions):
    """
        Calculates nucleotide diversity for all samples (total), snp, and length.
        this is defined as: sum(af)/possible genomic positions (stop codon - start codon)
    """
    for sample in results:
        results[sample]["nucleotide_diversity (cds)"] = results[sample]["af (cds)"] / possible_genomic_positions
        results[sample]["nucleotide_diversity (snp)"] = results[sample]["af (snp)"] / possible_genomic_positions
        results[sample]["nucleotide_diversity (lp)"] = results[sample]["af (lp)"] / possible_genomic_positions
    return results


def write_output(sample_dict, output_file):
    header = ["sample", "count", "coverage (variants)", "coverage (cds)", "richness", "richness (snp)",
              "richness (lp)", "complexity (cds)", "complexity (snp)", "complexity (lp)", "nucleotide_diversity (cds)",
              "nucleotide_diversity (snp)", "nucleotide_diversity (lp)"]
    with open(output_file, "w") as out:
        out.write("{}\n".format(",".join(header)))
        for sample in sample_dict:
            out.write("{},{}\n".format(sample, ",".join([str(sample_dict[sample][h]) for h in header[1:]])))


def main():
    # fill in programs that generate the results files
    parser = ArgumentParser(description="takes output from the gather_data script")
    parser.add_argument("-p", "-vphaser", dest="vphaser_file", type=str, default=None,
                        help="location of the variant vphaser file")
    parser.add_argument("-l", "-lofreq", dest="lofreq_file", default=None,
                        help="location of the variant lofreq output")
    parser.add_argument("--start", dest="start_codon", required=True, type=int, help="codon position to start analyzing")
    parser.add_argument("--stop", dest="stop_codon", required=True, type=int, help="codon position to stop analyzing")
    parser.add_argument("-pvalue", dest="p_value", default=None, type=int,
                        help="p-value to filter by")
    parser.add_argument("--frequency", dest="allele_frequency", default=None, type=int,
                        help="allele frequency to filter by")
    # required arguments
    required = parser.add_argument_group("required arguments")
    required.add_argument("-d", dest="depths_directory", required=True, help="directory where depths file is located")
    required.add_argument("-o", dest="output_file", required=True,
                          help="directory and file to store results")
    args = parser.parse_args()

    possible_genomic_positions = args.stop_codon - args.start_codon

    # One of the following files needs to exist, but not both
    if bool(args.vphaser_file) == bool(args.lofreq_file):
        print("Input from either vphaser or lofreq must be specified, but not both.")
        sys.exit()

    # gather the data
    if args.vphaser_file:
        with open(args.vphaser_file) as f:
            f.readline()  # discard header
            header = vphaser_header
            data = [process_vphaser_file(d) for d in f.readlines()]
    else:
        with open(args.lofreq_file) as f:
            f.readline()  # discard header
            header = lofreq_header
            data = [process_lofreq_table(d) for d in f.readlines()]

    data = input_to_dictionary(header, data, args.start_codon, args.stop_codon)
    results = {k: {} for k in data.keys()}

    # transition the set into a dictionary of dictionaries. These are the results we'll give back at the end
    results = calculate_variant_coverage(data, results)
    results = calculate_cds_coverage(args.depths_directory, results, args.start_codon, args.stop_codon)
    results = calculate_richness_per_sample(data, results)
    results = calculate_complexity_per_sample(data, results, possible_genomic_positions)
    results = sum_alelle_frequencies(data, results)
    results = calculate_nucleotide_diversity(results, possible_genomic_positions)

    write_output(results, args.output_file)


if __name__ == "__main__":
    main()
