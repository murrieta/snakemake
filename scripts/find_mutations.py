from argparse import ArgumentParser
from math import floor
from operator import itemgetter
import os
import sys

"""
Looking at consensus changes
The process:
1. Variants are screened using the following criteria:
   * For vphaser Strand-Bias above 0.05 is removed
   * Variants below 0.01 are removed
2. Check each mutant against the ZIKV model document to look for mutant types. Nonsynonymous change means that
   when

This script expects two files:
1. a csv file in the following format: position, nucleotide, polyprotein
2. output from nucleotide_analysis.py for either vphaser or lofreq

Example of how to run the program:
vphaser2:
     python scripts/find_mutations.py -p results/ZIKA/vphaser2/nucleotide_results.csv -f annotations/ZIKA/ZIKV_model_PRVABC59.csv  -o results/ZIKA/vphaser2/mutation_analysis.csv

lofreq:
     python scripts/find_mutations.py -l results/ZIKA/lofreq/nucleotide_results.csv -f annotations/ZIKA/ZIKV_model_PRVABC59.csv  -o results/ZIKA/lofreq/mutation_analysis.csv
"""

codon_table = {"TTT": "F", "TTC": "F", "TTA": "L", "TTG": "L",
               "TCT": "S", "TCC": "S", "TCA": "S", "TCG": "S",
               "TAT": "Y", "TAC": "Y", "TAA": "*", "TAG": "*",
               "TGT": "C", "TGC": "C", "TGA": "*", "TGG": "W",
               "CTT": "L", "CTC": "L", "CTA": "L", "CTG": "L",
               "CCT": "P", "CCC": "P", "CCA": "P", "CCG": "P",
               "CAT": "H", "CAC": "H", "CAA": "Q", "CAG": "Q",
               "CGT": "R", "CGC": "R", "CGA": "R", "CGG": "R",
               "ATT": "I", "ATC": "I", "ATA": "I", "ATG": "M",
               "ACT": "T", "ACC": "T", "ACA": "T", "ACG": "T",
               "AAT": "N", "AAC": "N", "AAA": "K", "AAG": "K",
               "AGT": "S", "AGC": "S", "AGA": "R", "AGG": "R",
               "GTT": "V", "GTC": "V", "GTA": "V", "GTG": "V",
               "GCT": "A", "GCC": "A", "GCA": "A", "GCG": "A",
               "GAT": "D", "GAC": "D", "GAA": "E", "GAG": "E",
               "GGT": "G", "GGC": "G", "GGA": "G", "GGG": "G"}


def check_nucleotide(codon, index, nucleotide):
    """ Function that checks to check whether a substitution will result in
        a synonymous or nonsynonymous mutation.
        A nonsynonymous mutation alters the amino acid sequence.
    Args:
        amino_acid (str): the amino acid
        index (int): range is from 0-2. The index of the nucleotide to change.
        nucleotide (str): the nucleotide that will be substituted in
    Returns:
        str: either S for synonymous or NS for nonsynonymous
    """
    original_codon = codon_table[codon]
    return "NS" if original_codon != codon_table[new_codon(codon, index, nucleotide)] else "S"


def new_codon(codon, index, nucleotide):
    """Takes a codon and replaces a new nucleotide at the given index."""
    return (codon[:index] + nucleotide + codon[index+1:]).upper()


def analyze_snps(data, ref_data):
    results = []
    for pos in data:
        for variant in data[pos]:
            if variant["type"] == "snp":
                pos = int(variant["pos"])  # figure out index
                if pos not in ref_data: continue
                nucleotide_index = int(ref_data[pos]["index"])  # save nucleotide index (0, 1, 2)
                ref_codon = ref_data[pos]['codon']  # get codon
                ref_aa = codon_table[ref_codon]
                alt_codon = new_codon(ref_codon, nucleotide_index, variant["alt"]).upper()
                alt_aa = codon_table[alt_codon]
                mutation_type = check_nucleotide(ref_codon, nucleotide_index, variant["alt"])
                # position, nucleotide_position, ref_codon, ref_aa, alt_codon, alt_aa, mutation (S or NS)
                results.append([variant["sample"], pos, nucleotide_index + 1, ref_codon, ref_aa, alt_codon, alt_aa, mutation_type, variant["coverage"], variant["af"]])
    return results


def in_coding_region(position, regions):
    for i in range(0, len(regions), 2):
        if regions[i] <= position < regions[i + 1]:
            return True
    return False


def process_model(data, alpha_coding_regions):
    processed_data = {}
    region = None
    for line_num, line in enumerate(data):
        fields = line.strip().split(",")
        if not fields[0]: continue
        key = int(fields[0])
        # don't need to consider codons in the non-coding region
        if fields[2] in ("5'UTR", "3'UTR", ""):
            continue
        if alpha_coding_regions is not None:
            if not in_coding_region(key, alpha_coding_regions):
                protein_position = 0  # polyprotein not relevant in this instance
                continue
        # will check to see if the polyprotein has changed
        if not region or region != fields[2]:
            region = fields[2]
            protein_position = 0

        nucleotide_index = protein_position % 3
        if nucleotide_index == 0:  # should be true
            codon = assemble_codon(data[line_num:line_num+3])

        processed_data[key] = {"codon": codon,
                               "index": nucleotide_index,
                               "position in polyprotein": floor(protein_position / 3) + 1,
                               "polyprotein type": region}
        protein_position += 1
    return processed_data


def file_to_dictionary(file):
    """
    process lines of vphaser file and calculate var_freq
    """
    results = {}
    with open(file) as f:
        header = f.readline().strip().split(",")
        for line in f:
            line = line.strip().split(",")
            pos = int(line[header.index("pos")])
            mutation = {}
            if pos not in results:
                results[pos] = []
            for h in header:
                if h in ("qual", "coverage", "entropy (S)", "af", "strd_bias_pval", "coverage", "var_perc"):
                    mutation[h] = float(line[header.index(h)])
                elif h in "sb":
                    mutation[h] = int(line[header.index(h)])
                else:
                    mutation[h] = line[header.index(h)]
            if 'af' not in mutation:
                mutation['af'] = mutation['var_perc']/100
            results[pos].append(mutation)
    return results


def assemble_codon(lines):
    return "".join([l.split(",")[1] for l in lines])


def main():
    parser = ArgumentParser(description="Further analysis on single nucleotide polymorphisms")
    parser.add_argument("-p", "--vphaser", dest="vphaser_file", type=str, default=None,
                        help="location of the vphaser file")
    parser.add_argument("-l", "--lofreq", dest="lofreq_file", default=None,
                        help="location of the lofreq file")
    parser.add_argument("--alpha", dest="alpha", type=str, default=None,
                        help="comma delimited list input of start and stop locations for the coding regions")
    required = parser.add_argument_group("required arguments")
    required.add_argument("-f", "--file", dest="model_file", type=str, required=True,
                          help="location of the model data")
    required.add_argument("-o", dest="output_file", required=True, help="location to store output")

    args = parser.parse_args()

    # One of the following files needs to exist
    if not args.vphaser_file and not args.lofreq_file:
        print("Input from either vphaser2 or lofreq must be specified")
        sys.exit()

    alpha = list(map(int, args.alpha.split(","))) if args.alpha else None

    with open(args.model_file) as f:
        f.readline()  # discard header
        model_ref = process_model(f.readlines(), alpha)

    if args.vphaser_file:
        data = file_to_dictionary(args.vphaser_file)
    else:
        data = file_to_dictionary(args.lofreq_file)

    results = sorted(analyze_snps(data, model_ref), key=itemgetter(0))

    with open(args.output_file, "w") as out:
        out.write("sample,position,nucleotide_position,ref_codon,ref_aa,alt_codon,alt_aa,mutation (S or NS),coverage,af\n")
        out.write("\n".join([",".join(map(str, r)) for r in results]))
        out.write("\n")


if __name__ == "__main__":
    main()
